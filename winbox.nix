{ config, pkgs, ... }:

let
  version = "3.37";
  winbox = builtins.fetchurl {
    url = "https://download.mikrotik.com/winbox/${version}/winbox64.exe";
    sha256 = "abe696e45809f26b0320926a0014d3088dcc5ac43d553a2b7a4e25f54a047439";
  };
in
{
  xdg.desktopEntries.winbox = {
    name = "winbox";
    exec = "${pkgs.wineWowPackages.stable}/bin/wine64 ${winbox}";
    terminal = false;
    categories = [ "Network" ];
  };
}
