{ config, pkgs, lib, ... }:

let
  inherit (pkgs.vscode-utils) buildVscodeMarketplaceExtension;

  vscode-extensions = {
    ymotongpoo.licenser = buildVscodeMarketplaceExtension rec {
      mktplcRef = {
        name = "licenser";
        publisher = "ymotongpoo";
        version = "1.7.0";
        sha256 = "0b8a7a40s9wc05kx0pwzfaimkdavi8mhikqf28davnnzzaq298nq";
      };
      meta = {
        license = lib.licenses.asl20;
      };
    };
    gitlab.gitlab-workflow = buildVscodeMarketplaceExtension rec {
      mktplcRef = {
        name = "gitlab-workflow";
        publisher = "gitlab";
        version = "3.40.0";
        sha256 = "0znhj4503hfq1ndrksqma0jl9xs5dlhw503b2mz9csl6snmq8ppf";
      };
      meta = {
        license = lib.licenses.mit;
      };
    };
  };
in
{
  home.packages = with pkgs; [
    nixpkgs-fmt
  ];

  programs.vscode = {
    enable = true;

    userSettings = {
      "update.mode" = "none";
      "editor.codeActionsOnSave" = {
        "source.organizeImports" = true;
      };
      "editor.formatOnSave" = true;
      "editor.tabSize" = 2;
      "editor.renderWhitespace" = "all";
      "files.autoSave" = "onWindowChange";
      "files.exclude" = {
        "**/.git" = true;
        "**/.svn" = true;
        "**/.hg" = true;
        "**/CVS" = true;
        "**/.DS_Store" = true;
        "**/Thumbs.db" = true;
        "**/node_modules" = true;
        "**/.venv*" = true;
        "**/venv*" = true;
        "/result" = true;
      };
      "files.insertFinalNewline" = true;
      "licenser.author" = "Fabrizio Lungo";
      "licenser.disableAutoHeaderInsertion" = true;
      "licenser.license" = "MIT";
      "python.formatting.provider" = "black";
      "python.linting.pylintEnabled" = true;
      "[python]" = {
        "editor.tabSize" = 4;
      };
      "[dockerfile]" = {
        "editor.defaultFormatter" = "ms-azuretools.vscode-docker";
      };
    };

    extensions = with pkgs.vscode-extensions; with vscode-extensions; [
      bbenoist.nix
      codezombiech.gitignore
      davidanson.vscode-markdownlint
      dbaeumer.vscode-eslint
      eamodio.gitlens
      editorconfig.editorconfig
      foxundermoon.shell-format
      gitlab.gitlab-workflow
      golang.go
      james-yu.latex-workshop
      jnoortheen.nix-ide
      ms-azuretools.vscode-docker
      ms-python.python
      ms-vscode.cpptools
      redhat.java
      timonwong.shellcheck
      valentjn.vscode-ltex
      ymotongpoo.licenser
      yzhang.markdown-all-in-one
    ];
  };
}
