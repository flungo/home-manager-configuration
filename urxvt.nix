{ config, pkgs, ... }:

{
  # Dependencies
  home.packages = with pkgs; [ nerdfonts ];

  # Install urxvt pluging by downloading
  home.file.".urxvt/ext/resize-font".source = builtins.fetchurl {
    # b5935806f159594f516da9b4c88bf1f3e5225cfd
    url = "https://raw.githubusercontent.com/simmel/urxvt-resize-font/master/resize-font";
    sha256 = "d84ab0a99f03a985465d93c5290ae9570ae073575e5045e446f467465d331552";
  };

  programs.urxvt = {
    enable = true;

    fonts = [ "xft:DejaVuSansM Nerd Font Mono:pixelsize=16:antialias=true:hinting=true" ];

    keybindings = {
      # Clipboard control
      "Shift-Control-C" = "eval:selection_to_clipboard";
      "Shift-Control-V" = "eval:paste_clipboard";

      # Font resizing
      "Control-Up" = "resize-font:bigger";
      "Control-plus" = "resize-font:bigger";
      "Control-Down" = "resize-font:smaller";
      "Control-minus" = "resize-font:smaller";
      "Control-equal" = "resize-font:reset";
      "Control-0" = "resize-font:reset";
      "Control-slash" = "resize-font:show";
    };

    iso14755 = false;

    scroll = {
      bar = {
        enable = false;
      };
      lines = 20000;
    };

    extraConfig = {
      lineSpace = 1;

      perl-ext = "";
      perl-ext-common = "default,confirm-paste,resize-font";

      # BG/FG
      background = "#2B2B2B";
      foreground = "#DEDEDE";

      # Underline
      colorUL = "#86a2b0";

      # black
      color0 = "#2E3436";
      color8 = "#555753";
      # red
      color1 = "#CC0000";
      color9 = "#EF2929";
      # green
      color2 = "#4E9A06";
      color10 = "#8AE234";
      # yellow
      color3 = "#C4A000";
      color11 = "#FCE94F";
      # blue
      color4 = "#3465A4";
      color12 = "#729FCF";
      # magenta
      color5 = "#75507B";
      color13 = "#AD7FA8";
      # cyan
      color6 = "#06989A";
      color14 = "#34E2E2";
      # white
      color7 = "#D3D7CF";
      color15 = "#EEEEEC";
    };
  };
}
