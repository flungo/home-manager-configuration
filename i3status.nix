{ config, pkgs, ... }:

{
  home.packages = with pkgs; [
    python310Packages.py3status
  ];

  programs.i3status = {
    enable = true;

    general = {
      colors = true;
      interval = 1;
    };

    modules = {
      "whatismyip" = {
        position = 1;
        settings = {
          format = "WAN: {ip} ({country})";
          button_toggle = 0;
          button_refresh = 1;
        };
      };
      # Replace wireless module with wifi module
      "wireless _first_" = {
        enable = false;
      };
      # TODO: Enable only if the system has WiFi
      "wifi _first_" = {
        position = 2;
        settings = {
          format = "W: {ssid} ({signal_percent}%/{bitrate}mbps/{ip})|W: down";
        };
      };
      # TODO: Enable only if the system has bluetooth
      "bluetooth" = {
        position = 4;
        settings = {
          format = "BT[: {format_device}]";
        };
      };
      # Disable default disk module, not working with py3status
      "disk /" = {
        enable = false;
        position = 72;
      };
      "lm_sensors" = {
        position = 74;
        settings = {
          # TODO: i3status doesn't support arrays (if making upstream contribution to fix, also allow override of package)
          #chips = "['coretemp-*', 'nouveau-*']";
          #sensors = "['core_*', 'temp*']";
          format_chip = "[\\?if=name=coretemp-isa-0000 CPU1 ][\\?if=name=coretemp-isa-0001 CPU2 ][\\?if=name=nouveau-pci-8500 GPU ]{format_sensor}";
          format_sensor = "[\\?color=auto.input&show {input}]";
          format_sensor_separator = ":";
        };
      };
      "load" = {
        position = 76;
      };
      "memory" = {
        position = 78;
        settings = {
          format = "%used/%available";
        };
      };
      "volume master" = {
        position = 85;
        settings = {
          format = "♪: %volume";
          format_muted = "♪: muted (%volume)";
          device = "pulse:combined";
        };
      };
      "backlight" = {
        position = 88;
      };
      "battery all" = {
        position = 90;
        settings = {
          hide_seconds = true;
        };
      };
      "tztime local" = {
        position = 99;
      };
    };
  };
}
