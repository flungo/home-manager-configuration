{ config, pkgs, ... }:

{
  xresources.properties = {
    "Xft.dpi" = 120;
  };
}
