{ config, pkgs, ... }:

{
  programs.git = {
    enable = true;

    userName = "Fabrizio Lungo";
    userEmail = "git@flungo.me";

    ignores = [
      "*~"
      "*.swp"
      "*.tmp"
      "*.orig"
      ".gitstats/"
      ".venv*/"
      "flungo-notes.md"
      "tags"
    ];

    aliases = {
      wdiff = "diff --color-words";
      cdiff = "diff --word-diff-regex=.";
      # git lg aliases from https://stackoverflow.com/questions/1838873/visualizing-branch-topology-in-git/34467298#34467298
      lg = "lg1";
      lg1 = "lg1-specific --all";
      lg2 = "lg2-specific --all";
      lg3 = "lg3-specific --all";
      lg1-specific = "log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(auto)%d%C(reset)'";
      lg2-specific = "log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset)%C(auto)%d%C(reset)%n''          %C(white)%s%C(reset) %C(dim white)- %an%C(reset)'";
      lg3-specific = "log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset) %C(bold cyan)(committed: %cD)%C(reset) %C(auto)%d%C(reset)%n''          %C(white)%s%C(reset)%n''          %C(dim white)- %an <%ae> %C(reset) %C(dim white)(committer: %cn <%ce>)%C(reset)'";
    };

    lfs.enable = true;

    extraConfig = {
      apply = {
        # Fix whitespace errors when applying a patch
        whitespace = "fix";
      };
      core = {
        autocrlf = "input";
      };
      commit = {
        verbose = true;
      };
      diff = {
        # Show moved lines
        colormoved = "plain";
        colormovedws = "allow-indentation-change";
        # Detect copies as well as renames
        renames = "copies";
      };
      help = {
        # Automatically correct and execute mistyped commands
        # With a value of 1, it will wait 0.1 second before proceding with the correction
        autocorrect = 1;
      };
      init = {
        defaultBranch = "develop";
      };
      merge = {
        # Include summaries of merged commits in newly created merge commit messages
        log = true;
      };
      pull = {
        rebase = true;
      };
      push = {
        default = "current";
      };
      rebase = {
        # Show commands in the todo list in their abbreviated form.
        # I always use abbreviated commands, might as well have them presented that way to begin with
        abbreviatecommands = true;
        # Automatic fixup, amend and squash when using rebase
        # Commits can be marked using the --fixup flag when comitting
        autosquash = true;
        # Fix whitespace errors when rebasing
        whitespace = "fix";
      };
      rerere = {
        enabled = true;
      };
    };
  };
}
