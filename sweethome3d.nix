{ ... }:

with import <nixpkgs> { }; let
  # Constants
  module = "SweetHome3D";

  # Create the sweethome2d package
  sweethome2d = stdenv.mkDerivation rec {
    pname = "sweethome2d-application";
    version = sweethome3d.application.version;
    src = sweethome3d.application;
    description = "${sweethome3d.application.description} (without 3D features)";
    exec = "sweethome2d";
    sweethome2dItem = makeDesktopItem {
      inherit exec;
      name = pname;
      desktopName = "Sweet Home 2D";
      icon = "sweethome3d-application";
      comment = description;
      genericName = "Computer Aided (Interior) Design";
      categories = [ "Graphics" "2DGraphics" ];
    };

    nativeBuildInputs = [ makeWrapper ];

    installPhase = ''
      runHook preInstall
      mkdir -p $out/bin $out/share/applications
      cp -r "${sweethome2dItem}/share/applications/"* $out/share/applications/
      # MESA_GL_VERSION_OVERRIDE is needed since the update from MESA 19.3.3 to 20.0.2:
      # without it a "Profiles [GL4bc, GL3bc, GL2, GLES1] not available on device null"
      # exception is thrown on startup.
      # https://discourse.nixos.org/t/glx-not-recognised-after-mesa-update/6753
      makeWrapper ${jre8}/bin/java $out/bin/$exec \
        --set MESA_GL_VERSION_OVERRIDE 2.1 \
        --prefix XDG_DATA_DIRS : "$XDG_ICON_DIRS:${gtk3.out}/share:${gsettings-desktop-schemas}/share:${sweethome3d.application}/share:$out/share:$GSETTINGS_SCHEMAS_PATH" \
        --add-flags "-Dsun.java2d.opengl=true -Dcom.eteks.sweethome3d.no3D=true -jar ${sweethome3d.application}/share/java/${module}-${version}.jar -cp ${sweethome3d.application}/share/java/Furniture.jar:${sweethome3d.application}/share/java/Textures.jar:${sweethome3d.application}/share/java/Help.jar -d${toString stdenv.hostPlatform.parsed.cpu.bits}"
      runHook postInstall
    '';

    dontStrip = true;

    meta = {
      homepage = sweethome3d.application.meta.homepage;
      inherit description;
      license = sweethome3d.application.meta.license;
      maintainers = sweethome3d.application.meta.maintainers;
      platforms = sweethome3d.application.meta.platforms;
    };
  };
in
{
  home.packages = [
    sweethome3d.application
    sweethome2d
  ];
}
