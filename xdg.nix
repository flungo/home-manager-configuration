{ config, pkgs, ... }:

{
  xdg = {
    mimeApps = {
      enable = true;

      defaultApplications = {
        "application/pdf" = "org.pwmt.zathura.desktop";
        "image/png" = "sxiv.desktop";
        "image/jpeg" = "sxiv.desktop";
        "inode/directory" = "pcmanfm.desktop";
        "text/html" = "google-chrome.desktop";
        "x-scheme-handler/about" = "google-chrome.desktop";
        "x-scheme-handler/discord-402572971681644545" = "discord-402572971681644545.desktop";
        "x-scheme-handler/http" = "google-chrome.desktop";
        "x-scheme-handler/https" = "google-chrome.desktop";
        "x-scheme-handler/mailto" = "google-chrome.desktop";
        "x-scheme-handler/msteams" = "teams.desktop";
        "x-scheme-handler/sgnl" = "signal-desktop.desktop";
        "x-scheme-handler/unknown" = "google-chrome.desktop";
      };
    };
  };
}
