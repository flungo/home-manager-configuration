{ config, pkgs, ... }:

{
  home.file.".xinitrc".text = ''
    [[ -f ~/.Xresources ]] && xrdb -merge -I$HOME ~/.Xresources
    [[ -f ~/.profile ]] && source ~/.profile
  '';
}
