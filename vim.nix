{ config, pkgs, ... }:

{
  home.packages = with pkgs; [ black isort nerdfonts nixfmt shfmt ];

  programs.vim = {
    enable = true;

    plugins = with pkgs.vimPlugins; [
      ale
      editorconfig-vim
      fzf-vim # fzf.vim
      vim-airline
      vim-closer
      vim-closetag
      vim-devicons
      vim-endwise
      vim-eunuch
      vim-gitgutter
      vim-gutentags
      vim-indent-guides
      nerdcommenter
      nerdtree
      nerdtree-git-plugin
      securemodelines
      syntastic
      tagbar
      vim-trailing-whitespace
    ];

    # TODO: Migate settings from extraConfig
    settings = { };

    extraConfig = ''
      " Set 'nocompatible' to ward off unexpected things that your distro might
      " have made, as well as sanely reset options when re-sourcing .vimrc
      set nocompatible

      " Attempt to determine the type of a file based on its name and possibly its
      " contents. Use this to allow intelligent auto-indenting for each filetype,
      " and for plugins that are filetype specific.
      filetype indent plugin on

      " Enable syntax highlighting
      syntax on

      " Theme and styling
      set t_Co=256
      set background=dark

      colorscheme desert

      hi SpellBad ctermbg=166 cterm=underline,bold,italic
      hi SpellCap ctermbg=none cterm=underline,bold
      hi SpellLocal ctermbg=none cterm=underline,italic
      hi SpellRare ctermbg=none cterm=italic

      " Allow switching from an unsaved buffer without saving it first.
      set hidden

      " Better command-line completion
      set wildmenu

      " Show partial commands in the last line of the screen
      set showcmd

      " Highlight searches (use <C-L> to temporarily turn off highlighting; see the
      " mapping of <C-L> below)
      set hlsearch

      " Use case insensitive search, except when using capital letters
      " Set the command window height to 2 lines, to avoid many cases of having to
      " "press <Enter> to continue"
      set cmdheight=2

      " Display line numbers on the left
      set number

      " Use relative numbering from the current line
      set relativenumber

      " Quickly time out on keycodes, but never time out on mappings
      set notimeout ttimeout ttimeoutlen=200

      " Use <F11> to toggle between 'paste' and 'nopaste'
      set pastetoggle=<F11>

      " Indentation settings for using 2 spaces instead of tabs.
      set shiftwidth=2
      set softtabstop=2
      set expandtab

      " Spell check
      set spell
      set spelllang=en_gb

      " Map Y to act like D and C, i.e. to yank until EOL, rather than act as yy,
      " which is the default
      map Y y$

      " Map <C-L> (redraw screen) to also turn off search highlighting until the
      " next search
      nnoremap <C-L> :nohl<CR><C-L>

      " Line wrapping
      set wrap
      set linebreak
      set breakindent breakindentopt=shift:4

      " Encoding
      set encoding=utf8

      " NERDTree Configuration

      " Bind Ctrl+\ to toggle the NERDTree
      map <C-\> :NERDTreeToggle<CR>

      " Open NERDTree automatically when a directory is opened with vim
      autocmd StdinReadPre * let s:std_in=1
      autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif

      " Close vim when the only window left open is a NERDTree
      autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

      " Tagbar Configuration

      " Bind Ctrl+m to toggle the Tagbar
      map <C-m> :TagbarToggle<CR>

      " Syntastic Configuration

      set statusline+=%#warningmsg#
      set statusline+=%{SyntasticStatuslineFlag()}
      set statusline+=%*

      let g:syntastic_always_populate_loc_list = 1
      let g:syntastic_auto_loc_list = 1
      let g:syntastic_check_on_open = 1
      let g:syntastic_check_on_wq = 0

      " Indent Guides Configuration

      let g:indent_guides_enable_on_vim_startup = 1

      " NERDCommenter Configuration

      nmap <C-_> <Plug>NERDCommenterToggle
      vmap <C-_> <Plug>NERDCommenterToggle<CR>gv

      " ALE (Asynchronous Lint Engine) Configuration
      " Configure fixers for ALE
      let g:ale_fixers = {}
      let g:ale_fixers.nix = ['nixfmt']
      let g:ale_fixers.python = ['black', 'isort']
      let g:ale_fixers.sh = ['shfmt']
      let g:ale_fix_on_save = 1
    '';
  };
}
