{ config, pkgs, ... }:

let
  unstable = import <nixos-unstable> { config = { allowUnfree = true; }; };
in
{
  home.packages = with pkgs; [
    discord
    dos2unix
    element-desktop
    ghostscript
    google-chrome
    imagemagick
    jetbrains.pycharm-community
    jq
    meld
    moreutils
    unstable.nomachine-client
    openfortivpn
    prusa-slicer
    qalculate-gtk
    qlcplus
    signal-desktop
    # tor-browser-bundle-bin
    universal-ctags
    vmware-horizon-client
    whatsapp-for-linux
    wpsoffice
    zoom-us
  ];
}
