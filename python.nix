{ config, pkgs, ... }:

{
  home.packages = with pkgs; [
    python310
    python310Packages.pip
  ];

  home.sessionVariables = {
    PIP_REQUIRE_VIRTUALENV = "true";
  };
}
