# Fabrizio's Home Manager Configuration

[Home Manager](https://github.com/nix-community/home-manager) configuration for Fabrizio Lungo (@flungo). This repo is only intended for use by Fabrizio, however can be used for examples (the quality of these examples may vary).

## Initial Setup

This repo requires that home-manager is installed. For me this is done with the following:

```bash
nix-channel --add https://github.com/nix-community/home-manager/archive/release-21.05.tar.gz home-manager
nix-channel --update
nix-shell '<home-manager>' -A install
```

## Usage

The `./home-manager` script acts as a wrapper for the system's home-manager binary ensuring that all commands use the configuration defined in this repo rather than the configuration at the default location for `home.nix`.

The configuration for the current home directory is updated by running the following command:

```bash
./home-manager switch
```