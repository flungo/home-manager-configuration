{ config, pkgs, ... }:

{
  home.packages = with pkgs; [
    arc-theme
  ];

  gtk = {
    enable = true;
    iconTheme = { name = "Arc"; };
    theme = { name = "Arc-Darker"; };
    font = { name = "DejaVu Sans"; size = 11; };
    gtk3 = {
      bookmarks = [
        "file://${config.home.homeDirectory}/images"
      ];
      extraConfig = {
        gtk-application-prefer-dark-theme = true;
      };
    };
  };
}