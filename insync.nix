{ config, pkgs, ... }:

{
  home.packages = with pkgs; [
    insync
  ];

  # Create a systemd unit for Insync
  # TODO: find the PID file so that systemd actually monitors it
  systemd.user.services.insync = {
    Unit = {
      Description = "Insync Headless";
      After = [
        "network.target"
        "systemd-user-sessions.service"
        "network-online.target"
      ];
    };
    Service = {
      Type = "oneshot";
      RemainAfterExit = true;
      ExecStart = "${pkgs.insync}/bin/insync start";
      ExecStop = "${pkgs.insync}/bin/insync quit";
      TimeoutSec = 30;
      TimeoutStopSec = 30;
      Restart = "on-failure";
      RestartSec = 30;
      StartLimitInterval = 350;
      StartLimitBurst = 10;
    };
    Install = {
      WantedBy = [ "default.target" ];
    };
  };
}