{ config, pkgs, ... }:

{

  home.packages = with pkgs; [
    # Virtual keyboard to allow emoji insertion
    xdotool
  ];

  programs.rofi = {
    enable = true;

    plugins = with pkgs; [ rofi-calc rofi-emoji ];

    extraConfig = {
      modi = "window,drun,run";
      sidebar-mode = true;
    };
  };
}
