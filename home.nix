{ config, pkgs, ... }:

{
  imports =
    [
      ./bash.nix
      ./fzf.nix
      ./git.nix
      ./gtk.nix
      ./i3.nix
      ./insync.nix
      ./packages.nix
      ./python.nix
      ./sweethome3d.nix
      ./syncthing.nix
      ./urxvt.nix
      ./vim.nix
      ./vscode.nix
      ./winbox.nix
      ./xdg.nix
      ./xserver.nix
    ];

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "fabrizio";
  home.homeDirectory = "/home/fabrizio";

  # Allow installation of non-free packages
  nixpkgs.config.allowUnfree = true;

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "21.05";

  # Configure the user environment with the current HOME_MANAGER_CONFIG
  # This should be stable as this configuration should only be reachable if HOME_MANAGER_CONFIG
  home.sessionVariables = {
    HOME_MANAGER_CONFIG = builtins.getEnv "HOME_MANAGER_CONFIG";
  };

  # Enable automatic upgrades with home-manager
  services.home-manager.autoUpgrade = {
    enable = true;
    frequency = "daily";
  };

  # Enable garbage collection (coming in 24.05)
  # nix.gc = {
  #   automatic = true;
  #   options = "--delete-older-than 30d";
  # };
}
