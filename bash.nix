{ config, pkgs, ... }:

{
  programs.bash = {
    enable = true;

    shellAliases = {
      nix-env-fzf = "nix-env -qa | fzf";
      nix-mpl-shell = "nix-shell -p gobjectIntrospection gtk3 'python310.withPackages(ps : with ps; [ matplotlib pygobject3 ipython ])'";
    };

    initExtra = ''
      docker-buildx-init() {
        docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
        docker buildx create --name multiarch --driver docker-container --use
        docker buildx inspect --bootstrap
      }

      ssh() (
        if [ "$TERM" == "rxvt-unicode-256color" ]; then
          export TERM=xterm-256color
        fi

        command ssh "$@"
      )
    '';
  };
}
